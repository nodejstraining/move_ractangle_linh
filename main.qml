import QtQuick 2.5
import QtQuick.Window 2.2
import QtQml.Models 2.1
import QtQuick.Controls 1.4

Window {
    visible: true
    width: 1340
    height: 660
    id: root
    title: qsTr("Image animation by possition")

    Image {
        anchors.fill: parent
        source: "/img/image/bk.png"
        //   sourceSize.width: 1024
        //   sourceSize.height: 1024
    }

    Rectangle{
        x: root.width/2 - 70
        y: root.height - 50
        width: 70
        height: 35
        color: "orange"
        radius: 8

        Text {
            anchors.centerIn: parent
            text: "start"
            font.pointSize: 10
        }

        MouseArea{
            anchors.fill: parent
            onClicked: {
                if(myRect.notrun){
                    myRect.state = 'move'
                    position.setInterval(4000)
                    myRect.notrun = false
                }
            }
        }
    }


    Rectangle{
        x: root.width/2 + 10
        y: root.height - 50
        width: 70
        height: 35
        color: "brown"
        radius: 8

        Text {
            anchors.centerIn: parent
            text: "reset"
            font.pointSize: 10
        }

        MouseArea{
            anchors.fill: parent
            onClicked: {
                myRect.posx = myRect.x
                myRect.posy = myRect.y
                myRect.state = "default"
                myRect.arrayx = []
                myRect.arrayy = []
                myRect.setpos = 0;
                myRect.index123 = 0;
                myRect.notrun = true
            }
        }
    }


    Rectangle
    {
        id: myRect
        property variant listsourceimg: ["img/image/11.png","img/image/12.png","img/image/13.png"]
        property var arrayx: []//luu toa do x
        property var arrayy: []//luu toa do y
        property int index123: 0 //bien chay mang tu dau toi cuoi mang
        property int setpos: 0//set do dai cua mang luu toa do
        property int index: 0//biến chạy của list listsourceimg
        property int repeat: 0//repeat để hình hoạt họa có chạy hay không
        property int posx: 0//thiet lap vi tri x cua ảnh
        property int posy: 0//thiết lập vị trí y của ảnh
        property bool notrun: true//thiết lập true khi reset và false khi start

        Image {
            id: image
            source: "img/image/11.png"
            height: 100
            width: 60

            Timer{
                id: settime
                interval: 200; running: true; repeat: true;
                onTriggered:{
                    myRect.index++

                    if(myRect.index > 2){
                        myRect.index = 0
                    }
                    image.source = myRect.listsourceimg[myRect.index]
                }
            }

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if(myRect.repeat % 2 == 0)
                        settime.running = true
                    else
                        settime.running = false

                    myRect.repeat ++
                }
            }
        }

        states: [
            State{
                name: "move"
                PropertyChanges{
                    target: myRect
                  //  x: position.x - 60
                //    y: position.y
                    x: myRect.arrayx[myRect.index123]
                    y: myRect.arrayy[myRect.index123]
                }
            },

            State{
                name: "default"
                PropertyChanges{
                    target: image
                    x: myRect.posx
                    y: myRect.posy
                  //  x:  myRect.arrayx[myRect.index123]
                 //   y:  myRect.arrayy[myRect.index123]
                }
            }

        ]

        transitions: [

            Transition {
                from: "*"
                to: "move"

                NumberAnimation{
                    properties: "x,y"
                    duration: 4000
                    easing.type: Easing.InOutQuad
                }
            }

        ]
    }

    Item {
        width: 70; height: 70

        DropArea {
            x: position.x - 10; y: position.y - 10
            width: 60; height: 60

            Rectangle {
                anchors.fill: parent
                color: "red"
                visible: parent.containsDrag
            }
        }

        Rectangle {
            x: 100; y: 40
            width: 50; height: 30
            color: "yellow"
            radius: 6
            id: position

            Text {
                id: rectname
                text: "go =>"
                font.pointSize: 10
                anchors.centerIn: parent
            }

            Drag.active: dragArea.drag.active
            Drag.hotSpot.x: 10
            Drag.hotSpot.y: 10

            Timer {
                id: timer
                running: false
                repeat: false

                onTriggered:{
                    myRect.state = "default";

                    if(myRect.setpos == 1){//truong hop dac biet khi nguoi dung chi chon 1 diem
                        myRect.posx = myRect.arrayx[myRect.index123]
                        myRect.posy = myRect.arrayy[myRect.index123]
                        myRect.state = 'move'
                    }

                    if(myRect.index123 < myRect.setpos - 1){
                        myRect.posx = myRect.arrayx[myRect.index123]
                        myRect.posy = myRect.arrayy[myRect.index123]
                        myRect.index123++
                        myRect.state = 'move'

                    }

                    if(myRect.index123 == myRect.setpos - 1){
                        timer.stop()
                      //  myRect.state = 'move' //co hay khong deu khong quan trong
                        console.log("khong dung")
                    }
                }
            }

            function setTimeout(delay)
            {
                if (timer.running) {
                    console.error("nested calls to setTimeout are not supported!");
                    return;
                }
                // note: an interval of 0 is directly triggered, so add a little padding
                timer.interval = delay + 1;
                timer.running = true;
                myRect.posx = position.x - 40
                myRect.posy = position.y - 20
            }


            function setInterval(delay)
            {
                if (timer.running) {
                    console.error("nested calls to setTimeout are not supported!");
                    return;
                }
                // note: an interval of 0 is directly triggered, so add a little padding
                timer.interval = delay + 1;
                timer.running = true;
                timer.repeat = true;
            }

            MouseArea {
                id: dragArea
                anchors.fill: parent
                drag.target: parent

                onClicked: {
                    if(myRect.arrayx[myRect.setpos-1] != position.x - 40 &&
                            myRect.arrayy[myRect.setpos-1] != position.y - 20){
                        myRect.arrayx[myRect.setpos] = position.x - 40
                        myRect.arrayy[myRect.setpos] = position.y - 20
                        myRect.setpos++

                    }else{
                        console.log("bi trung toa do. " + myRect.setpos)
                    }
                }

                onReleased: {//khi bỏ nhấn thì kích hoạt sự kiện

                }

            }
        }
    }

    /*Item {
        width: 200; height: 200

        Rectangle {
            anchors.centerIn: parent
            width: text.implicitWidth + 20; height: text.implicitHeight + 10
            color: "green"
            radius: 5

            Drag.active: dragArea.drag.active
            Drag.dragType: Drag.Automatic
            Drag.supportedActions: Qt.CopyAction
            Drag.mimeData: {
                "text/plain": "Copied text"
            }

            Text {
                id: text
                anchors.centerIn: parent
                text: "Drag me"
            }

            MouseArea {
                id: dragArea
                anchors.fill: parent

                drag.target: parent
                onPressed: parent.grabToImage(function(result) {
                    parent.Drag.imageSource = result.url
                })
            }
        }
    } */

   /* Rectangle{
        property int pos: 0
        property string name1: "aa"
        id: rect
        height: 20
        width: 40
        color: "black"

       MouseArea
       {
           anchors.fill: parent
           onClicked: {
               rect.pos++
               rect.name1 = "aa" +  rect.pos

           }
       }
    }


    Rectangle {
        x: 100
            Repeater { model: rect.pos
                Rectangle {
                    x: index*41
                    width: 40; height: 40
                    id:aaa
                    color: "lightgreen"
                    Text { text: index
                           font.pointSize: 30
                           anchors.centerIn: parent }
                }
            }
    } */
}
